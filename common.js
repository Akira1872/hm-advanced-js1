class Employee{
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get employeeName () {
        return this.name;
    }
    set employeeName (newName) {
        this.name = newName; 
    }
    get employeeAge () {
        return this.age;
    }
    set employeeAge (newAge) {
        this.age = newAge; 
    }
    get employeeSalary () {
        return this.salary;
    }
    set employeeSalary (newSalary) {
        this.salary = newSalary; 
    }
}


class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    get programmerSalary () {
        return this.salary * 3;
    }


}

console.log(new Programmer('Sahra', 23, '1200$', 'english'));
console.log(new Programmer('Kevin', 41, '4000$', 'arabic'));
console.log(new Programmer('Oliver', 29, '800$', 'latin'));
console.log(new Programmer('Kevin', 34, '1900$', 'polish'));
